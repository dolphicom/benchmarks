# benchmarks

Benchmarking algorithms in automatic recognition of marine animal calls.

In the spirit of the [Mittlemann benchmarks for optimization software](http://plato.la.asu.edu/bench.html).


## Data and Algos

The main dataset currently included is the [DCLDE 5th workshop dataset](http://www.mobysound.org/workshops_p2.html).

A few files from the [DCLDE Oahu 2022 dataset](http://www.soest.hawaii.edu/ore/dclde/dataset/) (NOAA PIFSC) are also included for [dolphicom](https://gitlab.com/dolphicom/dolphicom/).

Results saved in this repo as csv files containing: start/end time/frequency of detected tonal (4 fields) and tonal ID.

Time is rounded to the nearest 100th decimal place. Frequency is rounded to the nearest Hertz.

This information is sufficient to draw bounding boxes around tonals on spectrograms, which serves as a side-by-side comparison of algorithms.

Current detectors (whose data is stored as subfolders in this repo):

- `0-manual`: manually annotated bin files in `https://mobysound.org/5th_DCL_Expanded_eval_data.zip` available from [DCLDE 5th workshop dataset](http://www.mobysound.org/workshops_p2.html)
- `1-dolphicom`: results of [dolphicom](https://gitlab.com/dolphicom/dolphicom/)
- `2-pamguard`: results of [pamguard](https://www.pamguard.org/) v2.01.05
  - Used an 8 dB thresholding and added binary storage output to get results in files.
  - These detections go up to 50 kHz whereas `0-manual` and `1-dolphicom` cut off at 20 kHz.
  - Converted the binary files to csv using the R script `pamguard_bin2csv.R` in this repository.
  - Detailed steps [here](https://shadiakiki.notion.site/How-to-install-run-pamguard-for-dolphicom-benchmarks-2099a0a0ca7e45dd97e4d641567c4d43)


## Results on DCLDE 5th workshop, evaluation-2 dataset

The link below shows plots of spectrograms overlaid with each algorithm's detected whistle boxes:

https://dolphicom.s3.us-west-2.amazonaws.com/dolphicom_results/t11.3-f1-v20210819b-moby_5th_eval2-plotting_mobydiff_annotations-dolphicom_v052/html/index.html

Audio dataset:

- mobysound.org, 5th conference, evaluation-2 dataset ([link](http://www.mobysound.org/workshops_p2.html))

Box color legend:

- Red: manual annotations as documented at mobysound.org link above
- Green: pamguard 2.01.05
- Black: dolphicom 0.5.2


The data behind the bounding boxes are available as csv files in this repository.

To compare annotations of "manual", dolphicom, and "pamguard" for mobysound 5th workshop's DCL expanded eval dataset, in the terminal with [vimdiff](http://vimdoc.sourceforge.net/htmldoc/diff.html):

```
vimdiff    0-manual/5th_DCL_Expanded_eval_data/eval_data/Delphinus\ delphis/CC0707-TA04-070630-145000.csv \
        1-dolphicom/5th_DCL_Expanded_eval_data/eval_data/Delphinus\ delphis/CC0707-TA04-070630-145000.csv \
         2-pamguard/5th_DCL_Expanded_eval_data/eval_data/Delphinus\ delphis/CC0707-TA04-070630-145000.csv

# or on bash
vimdiff {0-manual,1-dolphicom,2-pamguard}/5th_DCL_Expanded_eval_data/eval_data/Delphinus\ delphis/CC0707-TA04-070630-145000.csv
```

To count the number of whistles (ie boxes) per source:

(Note that 0-manual and 2-pamguard generate lots of small boxes for whistle fragments, hence larger numbers as opposed to dolphicom)

```
wc -l {0-manual,1-dolphicom,2-pamguard}/5th_DCL_Expanded_eval_data/eval_data/Delphinus\ delphis/CC0707-TA04-070630-145000.csv
 220 0-manual
  47 1-dolphicom
 442 2-pamguard
 709 total

# another example
wc -l {0-manual,1-dolphicom,2-pamguard}/5th_DCL_Expanded_eval_data/eval_data/Delphinus\ capensis/CC0808-TA26-080826-163000.csv 
 639  0-manual
  58  1-dolphicom
  44  2-pamguard
```


## Citing the project

To cite the dolphicom benchmarks in academic publications, please use:

```
@misc{dolphicom/benchmarks,
  author = {Akiki, Shadi},
  title = {Dolphicom Benchmarks},
  year = {2021},
  publisher = {GitLab},
  journal = {GitLab repository},
  howpublished = {\url{https://gitlab.com/dolphicom/benchmarks}},
}
```
